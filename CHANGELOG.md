# Changelog


#### LogViewer-2.1.0 - 15/04/20:
    Accept a file name as input parameter (-f option)
    i.e.: LogViewer -f /var/tmp/ds.log/my.log

#### LogViewer-2.0.5 - 14/12/16:
    improve pom.xml file 

#### LogViewer-2.0.4 - 19/11/16:
    Fix typos

#### LogViewer-2.0.3 - 18/11/16:
    Mavenize

#### LogViewer-2.0.2 - 26/04/16:
    Fix a bug in time comparator

#### LogViewer-2.0.1 - 02/09/15:
    Change date format

#### LogViewer-2.0.0 - 08/07/15:
    Implement java device with JTango API.

#### LogViewer-1.2.3 - 08/07/15:
    last version using old java server API.

#### LogViewer-1.2.2 - 22/02/11:
    Initial Revision from svn

#### LogViewer-1.2.1 - 22/02/11:
    Initial Revision from svn
